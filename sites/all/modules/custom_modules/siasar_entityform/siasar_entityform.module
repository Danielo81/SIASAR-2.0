<?php

/**
 * Implements hook_views_api().
 */
function siasar_entityform_views_api() {
  return array(
    'api'  => 3,
    'path' => drupal_get_path('module', 'siasar_entityform') . '/views',
  );
}

/**
 * Implements hook_entity_info_alter().
 */
function siasar_entityform_entity_info_alter(&$entity_info) {
  $entity_info['entityform']['controller class'] = 'SiasarEntityformRevisionsController';
}

/**
 * Check if the given entityform ID has a record and a valid state.
 *
 * @param $entityform_id
 *
 * @return bool
 */
function _query_computable_revision($entityform_id) {
  $query_result     = db_select('entityform_computable_states', 'efcs')
    ->fields('efcs', ['entityform_id', 'computable'])
    ->condition('entityform_id', $entityform_id, '=')
    ->execute();
  $entityform_state = reset($query_result->fetchAllAssoc('computable'));
  if ($entityform_state) {
    return $entityform_state->computable;
  }
  return TRUE;
}

/**
 * Check if the given entityform ID has a record in the processing queue.
 *
 * @param $entityform_id int The entityform ID.
 *
 * @return bool
 */
function _query_form_in_queue($entityform_id) {
  $query_result     = db_select('entityform_computable_processing', 'efcp')
    ->fields('efcp', ['entityform_id', 'processed'])
    ->condition('entityform_id', $entityform_id, '=')
    ->execute();
  $state = reset($query_result->fetchAllAssoc('entityform_id'));
  return $state;
}

/**
 * Check if an entityform id has a validated revision
 *
 * @param $entityform_id int The entityform ID.
 *
 * @return boolean TRUE|FALSE
 */
function _query_validated_revision($entityform_id) {
  $entityform = entity_load_single('entityform', $entityform_id);
  $revisions  = $entityform && siasar_entityform_validated_revisions($entityform);
  return !empty($revisions);
}

/**
 * Reset the state for a given entityform ID.
 */
function _reset_computable_entityform($entityform_id) {
  db_merge('entityform_computable_states')
    ->key(array(
      'entityform_id' => $entityform_id,
    ))
    ->insertFields(array(
      'entityform_id'       => $entityform_id,
      'computable'          => 0,
      'entityform_revision' => NULL,
    ))
    ->execute();
}

/**
 * Function for check if the entityform has a valid revision and return the
 * revision if it is computable.
 *
 * @param $entityform_id integer The Entityform ID
 *
 * @return mixed The revision or NULL
 */
function _computable_revision($entityform_id) {

  $computable_revision = NULL;

  //Load the entity
  $entityform = entity_load_single('entityform', $entityform_id);

  if ($entityform) {

    //Reset the entityform state
    _reset_computable_entityform($entityform_id);

    //Check the entityform for the last valid revision
    $revisions = entityform_revision_list($entityform);

    foreach ($revisions as $revision) {
      $entityform_revision = entityform_get_revision($entityform, $revision->vid);

      if ($entityform_revision->field_status['und'][0]['value'] == SIASAR_ENTITYFORM_STATE_VALIDATED) {
        $computable_revision = $revision->vid;

        switch ($entityform_revision->bundle()) {
          case 'comunidad':
            // Check if the related form has system, service provider and household information
            if (isset($entityform_revision->field_com_viv_con_sistema[LANGUAGE_NONE])) {
              // Check all fieldcollections
              foreach ($entityform_revision->field_com_viv_con_sistema[LANGUAGE_NONE] as $fieldcollection_item) {
                $fc = entity_load_single('field_collection_item', $fieldcollection_item['value']);
                if ($fc) {

                  $valid_not_system_provider = empty($fc->field_system_ref[LANGUAGE_NONE][0]['target_id']) && empty($fc->field_prestador_servicio[LANGUAGE_NONE][0]['target_id']);

                  $valid_system_provider = !empty($fc->field_system_ref[LANGUAGE_NONE][0]['target_id']) && !empty($fc->field_prestador_servicio[LANGUAGE_NONE][0]['target_id']);
                  $valid_system_provider = $valid_system_provider && _query_validated_revision($fc->field_system_ref[LANGUAGE_NONE][0]['target_id']);
                  $valid_system_provider = $valid_system_provider && _query_validated_revision($fc->field_prestador_servicio[LANGUAGE_NONE][0]['target_id']);

                  if (!$valid_not_system_provider && !$valid_system_provider) {
                    $computable_revision = NULL;
                    break;
                  }
                }
              }
            }


            break;
          case 'prestador_de_servicio':
          case 'sistema':
            if (!find_validated_community_for_id($entityform_id, $entityform_revision->bundle() )) {
              $computable_revision = NULL;
            }
            break;
        }

        //Update records
        if ($computable_revision) {
          if ($entityform->computable_revision !== $computable_revision) {
            db_update('entityform_computable_states')
              ->fields([
                'computable'          => TRUE,
                'entityform_revision' => $computable_revision,
              ])
              ->condition('entityform_id', $entityform_id, '=')
              ->execute();

            // Trigger update
            switch ($entityform->bundle()) {
              case 'sistema':
                // Get all entities that were related in any revision to this entityform
                // getting that information from table: field_revision_field_system_ref

                flag_entityform_dependencies_for_processing($entityform_id, $entityform, 'field_revision_field_system_ref', 'field_system_ref_target_id');


                break;
              case 'prestador_de_servicio':
                // Get all entities that were related in any revision to this entityform
                // getting that information from table: field_prestador_servicio

                flag_entityform_dependencies_for_processing($entityform_id, $entityform, 'field_revision_field_prestador_servicio', 'field_prestador_servicio_target_id');

                break;
              case 'comunidad':
                //Flag all the field collections for all the revisions to be update
                flag_fieldcollection_dependencies_for_processing($entityform, $revisions);
                break;
              default:
                // nobody to update
                // This is for PAT, water quality, or testing
                break;
            }
          }

          // Detected last entityform revision computable, so stop here
          break;
        }
      }

    }

  }

  db_delete('entityform_computable_processing')
    ->condition('entityform_id', $entityform_id, '=')
    ->execute();

  //Return the revision or NULL
  return $computable_revision;
}

/**
 * Process dependecies from the current entiform revision validation.
 *
 * @param $entityform_id integer The Entityform ID
 * @param $entityform \Entityform The EntityForm
 *
 * @return array
 */
function flag_entityform_dependencies_for_processing($entityform_id, $entityform, $table, $target_field) {
  $query = "SELECT DISTINCT(eform_sistema.entity_id)
FROM {" . $table . "} as target
  INNER JOIN {field_collection_item} as field_collection ON target.revision_id = field_collection.revision_id
  INNER JOIN {field_revision_field_com_viv_con_sistema} as eform_sistema ON eform_sistema.field_com_viv_con_sistema_revision_id = field_collection.revision_id
  LEFT OUTER JOIN {entityform_computable_processing} as compute ON eform_sistema.entity_id = compute.entityform_id
WHERE target." . $target_field . " = :target_id
    AND compute.entityform_id IS NULL";

  $result      = db_query($query, [':target_id' => $entityform_id]);
  $process_ids = array_keys($result->fetchAllAssoc('entity_id'));

  foreach ($process_ids as $id) {
    $forms     = db_select('entityform_computable_processing', 'efcp')
      ->fields('efcp', ['entityform_id'])
      ->condition('entityform_id', $id, '=')
      ->execute();
    $insert_id = $forms->fetchAllAssoc('entityform_id');
    if (!$insert_id) {
      //Insert $id in entityform_computable_processing
      _update_processing_queue_element($entityform->entityform_id);
    }
  }
}

/**
 * Implements hook_cron().
 */
function siasar_entityform_cron() {
  $processing_items_amount = variable_get('siasar_entityform_processing_items_amount', 30);
  siasar_process_pending_computable_entityforms($processing_items_amount);
}

/**
 * @param $items integer Number of elements for the query limit
 *
 * @return int Elements processed
 */
function siasar_process_pending_computable_entityforms($items = 30) {
  $count = 0;

  if (lock_acquire('siasar_entityform_computable_process')) {
    //Set execution time
    variable_set('siasar_form_last_execution_time', format_date(time()));

    $entityforms = db_select('entityform_computable_processing', 'efcp')
      ->fields('efcp', ['entityform_id', 'processed'])
      ->range(0, $items)
      ->orderBy('processed', 'ASC')
      ->execute();

    foreach (array_keys($entityforms->fetchAllAssoc('entityform_id')) as $item) {
      _computable_revision($item);
      $count++;
    }

    lock_release('siasar_entityform_computable_process');
  }

  return $count;
}

/**
 * Implements hook_query_TAG_alter().
 *
 * Alter the query for retrieving only the computable entityforms.
 *
 */
function siasar_entityform_query_services_index_entityform_alter(QueryAlterableInterface $query) {
  $computable = isset($_GET['computable'])
    ? $_GET['computable']
    : 1;

  if ($query instanceof SelectQueryInterface && $computable != 'all') {
    $computable = (int) $computable;
    $query->innerJoin('entityform_computable_states', 'efcs', 'efcs.entityform_id = entityform.entityform_id');

    $where = 'efcs.computable = ' . $computable;

    if ($computable == 1) {
      $where .= ' and entityform.vid = efcs.entityform_revision';
    }
    $query->where($where);
  }
}

/**
 * Find a validated community for a given revision id of an entityform.
 *
 * @param $entityform_id . integer Revision id.
 *
 * @param $bundle . String The type of the form
 *
 * @return bool TRUE|FALSE
 */
function find_validated_community_for_id($entityform_id, $bundle) {

  $query = "SELECT community.entityform_id
FROM {entityform} as community
  INNER JOIN {field_data_field_com_viv_con_sistema} AS fc_field ON fc_field.revision_id = community.vid
  INNER JOIN {field_collection_item} AS fc ON fc_field.field_com_viv_con_sistema_revision_id = fc.revision_id";
           if ($bundle == 'prestador_de_servicio') {
             $query .= " inner JOIN {field_revision_field_prestador_servicio} AS fci on fci.revision_id = fc.revision_id";
             $column = "field_prestador_servicio_target_id";
           }
           elseif ($bundle == 'sistema') {
             $query .= " inner JOIN {field_revision_field_system_ref} AS fci on fci.revision_id = fc.revision_id";
             $column = "field_system_ref_target_id";
           }
           else {
             return FALSE;
           }

  $query .= " inner join {entityform_computable_states} as computable on computable.entityform_id = community.entityform_id
where
  fci.". $column ." = :id";

  $result = db_query($query, [':id' => $entityform_id]);
  $form_id = reset($result->fetchAllAssoc('entityform_id'));

  if ($form_id) {
    return _query_validated_revision($form_id->entityform_id);
  }

  return FALSE;
}

/**
 * Flag an entityform ID to be proccesed to check if it is computable.
 *
 * @param $entityform_id integer The entityform ID.
 */
function flag_fieldcollection_dependencies_for_processing($entityform, $revisions) {
  foreach ($revisions as $revision) {
    $entityform_revision = entityform_get_revision($entityform, $revision->vid);
    if (isset($entityform_revision->field_com_viv_con_sistema[LANGUAGE_NONE])) {
      foreach ($entityform_revision->field_com_viv_con_sistema[LANGUAGE_NONE] as $fieldcollection_item) {
        $fc = entity_load_single('field_collection_item', $fieldcollection_item['value']);
        if ($fc) {
          //Trigger system and service providers for update.
          _update_processing_queue_element($fc->field_system_ref[LANGUAGE_NONE][0]['target_id']);
          _update_processing_queue_element($fc->field_prestador_servicio[LANGUAGE_NONE][0]['target_id']);
        }
      }
    }
  }
}

/**
 * Insert an element in the queue in case it is not included yet.
 * @param $entityform_id int The entityform ID.
 */
function _update_processing_queue_element($entityform_id) {
  if ($entityform_id != NULL && !_query_form_in_queue($entityform_id)) {
    db_merge('entityform_computable_processing')
      ->key(array(
        'entityform_id' => $entityform_id,
      ))
      ->insertFields(array(
        'entityform_id' => $entityform_id,
        'processed'     => date(time()),
      ))
      ->execute();
  }
}

/**
 * Reset all the entityform states and proceesing queue.
 */
function reset_entityform_states() {
  //Truncate tables.
  $truncate = db_truncate('entityform_computable_states')->execute();
  $truncate = db_truncate('entityform_computable_processing')->execute();
  variable_del('siasar_form_last_execution_time');

  $entityform_ids = db_select('entityform', 'ef')
    ->fields('ef', array('entityform_id'))
    ->orderBy('entityform_id', 'ASC')
    ->execute();

  $entity_forms = $entityform_ids->fetchAllAssoc('entityform_id');
  foreach ($entity_forms as $entityform) {
    db_merge('entityform_computable_states')
      ->key(array(
        'entityform_id' => $entityform->entityform_id,
      ))
      ->insertFields(array(
        'entityform_id'       => $entityform->entityform_id,
        'computable'          => 0,
        'entityform_revision' => NULL,
      ))
      ->execute();
    db_merge('entityform_computable_processing')
      ->key(array(
        'entityform_id' => $entityform->entityform_id,
      ))
      ->insertFields(array(
        'entityform_id' => $entityform->entityform_id,
        'processed'     => 0,
      ))
      ->execute();
  }
}
